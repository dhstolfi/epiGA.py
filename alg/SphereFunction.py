#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# file  :  SphereFunction.py
# author:  Daniel H. Stolfi
# date  :  2019-08-15
#
# Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics.
# In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
# doi> 10.1016/j.ins.2017.10.005
# -*- coding: utf-8 -*-
"""
Example of the epiGenetic Algorithm solving the Sphere Function problem (Minimization, Float Representation).
"""

from epiga.problems.floatproblem import FloatProblem
from epiga.epiga import EpiGA
from epiga.operators.nucleosomegenerator import NucleosomeGenerator
from epiga.operators.binarytournamentselection import BinaryTournamentSelection
from epiga.operators.nucleosomebasedreproduction import NucleosomeBasedReproduction
from epiga.operators.numericgenesilencing import NumericGeneSilencing
from epiga.operators.elitistreplacement import ElitistReplacement


class SphereFunctionProblem(FloatProblem):
    """Sphere Function Problem."""

    def __init__(self, size, k_num):
        """__init__(int, float) -> None

        Sphere Function Problem constructor.

        Parameters:
            :size: the number of variables (dimension) of the problem
            :k_num: the numerator for calculating k term
        """
        super().__init__("Sphere Function", size, [-1000] * size, [1000] * size, [0.5] * size,
                         FloatProblem.MINIMIZATION)
        self.__k_num = k_num

    def _get_fitness(self, solution):
        return sum([x ** 2 for x in solution])

    def k(self, position):
        return self.__k_num / self.evaluations


if __name__ == '__main__':
    L = 20
    EV = 200000
    p = SphereFunctionProblem(L, int(round(EV / 1800)))
    alg = EpiGA(p, 100, 10, NucleosomeGenerator(1 / L, 2), BinaryTournamentSelection(),
                NucleosomeBasedReproduction(), NumericGeneSilencing(2 / L), ElitistReplacement(duplicates=False))
    # quiet run. Prints the best fitness at the end.
    print(alg.run(EV, 0))
