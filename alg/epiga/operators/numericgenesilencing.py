# -*- coding: utf-8 -*-
# file  :  numericgenesilencing.py
# author:  Daniel H. Stolfi
# date  :  2019-08-15
#
# Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics.
# In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
# doi> 10.1016/j.ins.2017.10.005

import random
from .epigeneticoperator import EpigeneticOperator


class NumericGeneSilencing(EpigeneticOperator):
    """
    Numeric Gene Silencing Operator.
    Based on Chelouah et al., 2000.
    """

    def __init__(self, p_e):
        """
        Constructor.

        :param float p_e: the epigenetic probability
        """
        super().__init__(p_e)

    def methylate(self, pop):
        p = pop.problem
        env = p.environment
        for i in range(pop.size):
            for cell in pop.get(i).cells:
                for j in range(len(cell.solution)):
                    if cell.nucleosomes[j] and random.random() < self._p_e:
                        v_min, v_max = p.limits(j)
                        delta = p.delta(j)
                        if random.random() < env[j]:
                            delta = -1 * delta
                        cell.solution[j] = max(min(cell.solution[j] + delta, v_max), v_min)
