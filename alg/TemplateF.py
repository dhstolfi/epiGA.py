#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# file  :  TemplateF.py
# author:  Daniel H. Stolfi
# date  :  2019-08-15
#
# Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics.
# In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
# doi> 10.1016/j.ins.2017.10.005
# -*- coding: utf-8 -*-
"""
Template example for defining a new float problem and solving it by using the epiGenetic Algorithm.
"""
import math
from epiga.epiga import EpiGA
from epiga.problems.floatproblem import FloatProblem
from epiga.operators.nucleosomegenerator import NucleosomeGenerator
from epiga.operators.binarytournamentselection import BinaryTournamentSelection
from epiga.operators.nucleosomebasedreproduction import NucleosomeBasedReproduction
from epiga.operators.numericgenesilencing import NumericGeneSilencing
from epiga.operators.elitistreplacement import ElitistReplacement


class MyProblem(FloatProblem):

    def __init__(self, size):
        """
        Problem Initialization.

        :param int size: the problem size. The number of float numbers in the solution vector (list).
        """
        lower_bounds = [-5.0] * size
        upper_bounds = [5.0] * size
        environment = [0.5] * size
        super().__init__("My Float Problem", size, lower_bounds, upper_bounds, environment, FloatProblem.MINIMIZATION)

    def _get_fitness(self, solution):
        """
        Implements the Fitness calculation.
        Evaluates the solution made of a list of float numbers of length equal to the problem size.

        :param list[float] solution: the solution vector. A list of float numbers.
        :return: the fitness value.
        :rtype: float
        """
        return sum([math.pow(x, 4) - 16 * math.pow(x, 2) + 5 * x for x in solution]) / 2.0

    def k(self, position):
        """
        Overrides the attenuation coefficient. Controls the convergence speed.

        :param int position: the position in the solution vector (list).
        :return: the fitness value.
        :rtype: float
        """
        return 200 / self.evaluations


if __name__ == '__main__':
    L = 10

    p = MyProblem(L)

    alg = EpiGA(p, 100, 5,
                NucleosomeGenerator(2 / L, 1),
                BinaryTournamentSelection(),
                NucleosomeBasedReproduction(),
                NumericGeneSilencing(1 / L),
                ElitistReplacement(duplicates=False))

    alg.run(30000, 3, "solution.txt", "stats.txt")
