#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# file  :  EggholderFunction.py
# author:  Daniel H. Stolfi
# date  :  2019-08-15
#
# Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics.
# In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
# doi> 10.1016/j.ins.2017.10.005
# -*- coding: utf-8 -*-
"""
Example of the epiGenetic Algorithm solving the Eggholder Function problem (Minimization, Integer Representation).
"""
import math

from epiga.problems.integerproblem import IntegerProblem
from epiga.epiga import EpiGA
from epiga.operators.nucleosomegenerator import NucleosomeGenerator
from epiga.operators.binarytournamentselection import BinaryTournamentSelection
from epiga.operators.nucleosomebasedreproduction import NucleosomeBasedReproduction
from epiga.operators.numericgenesilencing import NumericGeneSilencing
from epiga.operators.elitistreplacement import ElitistReplacement


class EggholderFunctionProblem(IntegerProblem):
    """Eggholder Function Problem."""

    def __init__(self, k_num):
        """
        Constructor.

        :param float k_num: the numerator for calculating k term.
        """
        super().__init__("Eggholder Function", 2, [-512, -512], [512, 512], [0.5, 0.5], IntegerProblem.MINIMIZATION)
        self.__k_num = k_num

    def _get_fitness(self, solution):
        x = solution[0]
        y = solution[1]
        return -1.0 * (y + 47) * math.sin(math.sqrt(abs(x / 2 + (y + 47)))) - x * math.sin(math.sqrt(abs(x - (y + 47))))

    def k(self, position):
        return self.__k_num / self.evaluations


if __name__ == '__main__':
    p = EggholderFunctionProblem(10)
    alg = EpiGA(p, 100, 1, NucleosomeGenerator(0.5, 0), BinaryTournamentSelection(),
                NucleosomeBasedReproduction(), NumericGeneSilencing(0.5), ElitistReplacement(duplicates=False))
    alg.run(30000, 3, "solution.txt", "stats.txt")
