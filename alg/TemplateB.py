#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# file  :  TemplateB.py
# author:  Daniel H. Stolfi
# date  :  2019-08-15
#
# Daniel H. Stolfi and Enrique Alba. Epigenetic algorithms: A New way of building GAs based on epigenetics.
# In: Information Sciences, vol. 424, Supplement C, pp. 250–272, 2018.
# doi> 10.1016/j.ins.2017.10.005
# -*- coding: utf-8 -*-
"""
Template example for defining a new binary problem and solving it by using the epiGenetic Algorithm.
"""
import random
from epiga.epiga import EpiGA
from epiga.problems.binaryproblem import BinaryProblem
from epiga.operators.nucleosomegenerator import NucleosomeGenerator
from epiga.operators.binarytournamentselection import BinaryTournamentSelection
from epiga.operators.nucleosomebasedreproduction import NucleosomeBasedReproduction
from epiga.operators.binarygenesilencing import BinaryGeneSilencing
from epiga.operators.elitistreplacement import ElitistReplacement


class MyProblem(BinaryProblem):

    def __init__(self, size):
        """
        Problem Initialization.

        :param int size: the problem size. The number of binary positions in the solution vector (list).
        """
        environment = [0.55] * size

        super().__init__("My Binary Problem", size, environment, BinaryProblem.MAXIMIZATION)

        self.__w = [random.randint(1, 10) for _ in range(size)]
        self.__b = [random.randint(1, 10) for _ in range(size)]
        self.__m = 5 * size

    def _get_fitness(self, solution):
        """
        Implements the Fitness calculation.
        Evaluates the solution made of a list of bool values of length equal to the problem size.

        :param list[bool] solution: the solution vector. A list of bool values.
        :return: the fitness value.
        :rtype: float
        """
        b = 0
        w = 0
        for i in range(len(solution)):
            if solution[i]:
                b += self.__b[i]
                w += self.__w[i]
        if w > self.__m:
            return 0
        else:
            return b


if __name__ == '__main__':
    L = 100

    p = MyProblem(L)

    alg = EpiGA(p, 20, 1,
                NucleosomeGenerator(3 / L, int(L / 6)),
                BinaryTournamentSelection(),
                NucleosomeBasedReproduction(),
                BinaryGeneSilencing(2 / L),
                ElitistReplacement(duplicates=False))

    alg.run(1000, 3, "solution.txt", "stats.txt")
